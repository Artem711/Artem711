## Who am I?
- 👨‍💻 Full-Stack TypeScript software developer and Mathematics enthusiast!
- 📖 Languages background: 🇷🇺 🇬🇧 🇪🇸 🇩🇪
- 📚 IB (International Baccalaureate) Student and mathematics lover.
- 👷🏻‍♂️ Passionate about writing clean solid code and building great products to solve people's problems at scale.
- ⚙ Open Source Believer and Contributor.

### I am specialised in building:
-  👨‍💻  **Frontend apps** - (**React JS / Next JS / Graph QL / Redux / Firebase / Graph CMS / D3.js**) development
- 👨‍🔬  **Backend apps** - (**Node JS / Nest JS / Graph QL / MongoDB / PostgreSQL / TypeORM**) development
- 📱  **Mobile apps** (iOS & Android) - (**React Native / Graph QL / Redux / Firebase / D3.js**) development

### Languages & Frameworks: 
- **JavaScript** <img align="left" alt="javascript" width="20px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/javascript.svg"/> - (React JS / React Native / Node JS / Blockchain)
- **TypeScript** <img align="left" alt="javascript" width="20px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/typescript.svg"/> - (React JS / React Native / Node JS / Blockchain)

## Links
- Gmail: swift.uix@gmail.com
- [Website](https://myportfolio-dev.web.app/)
- [GitHub](https://github.com/Artem711)
- [LinkedIn](https://www.linkedin.com/in/artem77/)
- [Twitter](https://twitter.com/Artem66063023)
- [YouTube](https://www.youtube.com/channel/UC2Q2qLKUSXfPS_mxrtqvixA)
